package com.example.test6

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.test6.databinding.ItemLayoutBinding

class ModelAdapter : RecyclerView.Adapter<ModelAdapter.ViewHolder>() {

    var list: List<Model.Content> = listOf()


    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.title.text = list[adapterPosition].titleKA
            binding.description.text = list[adapterPosition].descriptionKA
            binding.publisherData.text = list[adapterPosition].publish_date
            Glide.with(binding.image).load(list[adapterPosition].cover).into(binding.image)

        }

    }

    override fun getItemCount() = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }

    fun setData(newList: List<Model.Content>) {
        list = newList
        notifyDataSetChanged()
    }
}
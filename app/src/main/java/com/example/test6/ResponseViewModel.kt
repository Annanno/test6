package com.example.test6

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Response

class ResponseViewModel : ViewModel() {

    val responseList: MutableLiveData<Response<Model>> = MutableLiveData()
    var isLoading : MutableLiveData<Boolean> = MutableLiveData()

    fun getData() {
        viewModelScope.launch {
            isLoading.value = true
//            delay(5000)
            responseList.value = RetrofitInstance.retrofit.getData()
            isLoading.value = false

        }

    }


}
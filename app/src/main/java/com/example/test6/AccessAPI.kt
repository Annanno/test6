package com.example.test6

import retrofit2.Response
import retrofit2.http.GET

interface AccessAPI {

    @GET("v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab")
    suspend fun getData(): Response<Model>
}
package com.example.test6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var modelAdapter: ModelAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecyclerView()

        val viewModel = ViewModelProvider(this).get(ResponseViewModel::class.java)


        viewModel.getData()
        viewModel.isLoading.observe(this,{
            binding.progressBar.isVisible = it
        })

        viewModel.responseList.observe(this, { response ->
            if (response.isSuccessful)
                modelAdapter.setData(response.body()!!.content)
            else Toast.makeText(this, response.code(), Toast.LENGTH_SHORT).show()


        })


    }

    private fun initRecyclerView() {
        modelAdapter = ModelAdapter()
        binding.recycler.adapter = modelAdapter
        binding.recycler.layoutManager = LinearLayoutManager(this)


    }
}



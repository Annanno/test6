package com.example.test6

data class Model(
    val content: List<Content>
){
    data class Content(
        val category: String,
        val cover: String,
        val created_at: Long,
        val descriptionEN: String,
        val descriptionKA: String,
        val descriptionRU: String,
        val id: String,
        val isLast: Boolean,
        val publish_date: String,
        val published: Int,
        val titleEN: String,
        val titleKA: String,
        val titleRU: String,
        val updated_at: Long

    )
}